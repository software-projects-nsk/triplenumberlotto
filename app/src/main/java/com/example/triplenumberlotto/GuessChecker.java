package com.example.triplenumberlotto;

import android.content.Intent;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class GuessChecker extends AppCompatActivity {

    private Random rand = new Random();
    private Intent intent;
    private int totalScore, roundsLeft;

    // Integer variables and TextViews where they will be displayed
    int genNumber1, genNumber2, genNumber3;
    TextView genNumber1View, genNumber2View, genNumber3View;

    // Guessed Integer Variables and TextViews where they will be displayed
    int guessNumber1, guessNumber2, guessNumber3;
    TextView guessNumber1View, guessNumber2View, guessNumber3View;

    TextView roundScoreView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guess_checker);
        intent = getIntent();
        totalScore = intent.getIntExtra("totalScore", 99999);
        roundsLeft = intent.getIntExtra("roundsLeft", 0);

        SetRandomNumbers();
        CompareNumbers();
    }

    private int GenerateNumber() {
        return rand.nextInt(10);
    }

    private void AssignGuesses() {
        guessNumber1 = intent.getIntExtra("guessNumber1", 9);
        guessNumber2 = intent.getIntExtra("guessNumber2", 9);
        guessNumber3 = intent.getIntExtra("guessNumber3", 9);

        guessNumber1View = (TextView) findViewById(R.id.l_guessedNumber1);
        guessNumber2View = (TextView) findViewById(R.id.l_guessedNumber2);
        guessNumber3View = (TextView) findViewById(R.id.l_guessedNumber3);

        guessNumber1View.setText(Integer.toString(guessNumber1));
        guessNumber2View.setText(Integer.toString(guessNumber2));
        guessNumber3View.setText(Integer.toString(guessNumber3));
    }

    private void SetRandomNumbers() {
        genNumber1 = GenerateNumber();
        genNumber2 = GenerateNumber();
        genNumber3 = GenerateNumber();

        genNumber1View = (TextView) findViewById(R.id.l_genNumber1);
        genNumber2View = (TextView) findViewById(R.id.l_genNumber2);
        genNumber3View = (TextView) findViewById(R.id.l_genNumber3);

        genNumber1View.setText(Integer.toString(genNumber1));
        genNumber2View.setText(Integer.toString(genNumber2));
        genNumber3View.setText(Integer.toString(genNumber3));
    }

    private void CompareNumbers() {
        AssignGuesses();
        int matches = 0;

        if (genNumber1 == guessNumber1) {
            matches++;
            guessNumber1View.setTextColor(0xFF00FF00);
        } else {
            guessNumber1View.setTextColor(0xFFFF0000);
        }
        if (genNumber2 == guessNumber2) {
            matches++;
            guessNumber2View.setTextColor(0xFF00FF00);
        } else {
            guessNumber2View.setTextColor(0xFFFF0000);
        }
        if (genNumber3 == guessNumber3) {
            matches++;
            guessNumber3View.setTextColor(0xFF00FF00);
        } else {
            guessNumber3View.setTextColor(0xFFFF0000);
        }

        AssignScore(CalculateRoundScore(matches));
        totalScore += CalculateRoundScore(matches);
        roundsLeft--;
    }

    private void AssignScore(int roundScore) {
        roundScoreView = (TextView) findViewById(R.id.v_roundScore);
        roundScoreView.setText(Integer.toString(roundScore));
    }

    private int CalculateRoundScore(int matches) {
        return matches * matches * 10;
    }

    public void returnScore(View view) {
        Intent replyIntent = new Intent();
        replyIntent.putExtra("totalScore", totalScore);
        replyIntent.putExtra("roundsLeft", roundsLeft);
        setResult(RESULT_OK, replyIntent);
        finish();
    }
}
