package com.example.triplenumberlotto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int totalScore = 0;
    int roundsLeft = 25;

    private int number1, number2, number3;
    private EditText et_number1, et_number2, et_number3;
    private TextView et_totalScore, et_roundsLeft;

    private Button takeGuessButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et_totalScore = (TextView) findViewById(R.id.v_totalScore);
        et_roundsLeft = (TextView) findViewById(R.id.v_guessesLeft);
        takeGuessButton = (Button) findViewById(R.id.b_makeGuess);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                totalScore = data.getIntExtra("totalScore", totalScore);
                roundsLeft = data.getIntExtra("roundsLeft", roundsLeft);
                et_totalScore.setText(Integer.toString(totalScore));
                et_roundsLeft.setText(Integer.toString(roundsLeft));
                if (roundsLeft == 0)
                    takeGuessButton.setText("Restart Game?");
                ResetNumbers();
            }
        }
    }

    public void loadGuessCheckerActivity(View view) {
        if (roundsLeft > 0) {
            AssignGuesses();
            Intent intent = new Intent(this, GuessChecker.class);
            intent.putExtra("guessNumber1", number1);
            intent.putExtra("guessNumber2", number2);
            intent.putExtra("guessNumber3", number3);
            intent.putExtra("totalScore", totalScore);
            intent.putExtra("roundsLeft", roundsLeft);
            startActivityForResult(intent, 1);
        } else {
            this.recreate();
        }
    }

    private void AssignGuesses() {
        et_number1 = (EditText) findViewById(R.id.i_number1);
        et_number2 = (EditText) findViewById(R.id.i_number2);
        et_number3 = (EditText) findViewById(R.id.i_number3);

        number1 = !et_number1.getText().toString().equals("") ? Integer.parseInt(et_number1.getText().toString()) : 0;
        number2 = !et_number2.getText().toString().equals("") ? Integer.parseInt(et_number2.getText().toString()) : 0;
        number3 = !et_number3.getText().toString().equals("") ? Integer.parseInt(et_number3.getText().toString()) : 0;
    }

    private void ResetNumbers() {
        et_number1.setText("");
        et_number2.setText("");
        et_number3.setText("");
        et_number1.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
}
